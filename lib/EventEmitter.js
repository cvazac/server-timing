'use strict';

class EventEmitter {
    constructor() {
        this.listeners = new Map();
    }

    isFunction (obj) {
        return typeof obj === 'function' || false;
    }

    addListener(event, callback) {
        const listeners = this.listeners;

        listeners.has(event) || listeners.set(event, []);

        listeners.get(event).push(callback);
    }

    removeListener(event, callback) {
        const listeners = this.listeners;
        const callbacks = listeners.get(event);

        if (Array.isArray(callbacks) && callbacks.length) {
            const index = callbacks.reduce((i, listener, index) => {
                return (this.isFunction(listener) && listener === callback) ?
                    i = index :
                    i;
            }, -1);

            if (index > -1) {
                callbacks.splice(index, 1);

                listeners.set(event, callbacks);

                return true;
            }
        }

        return false;
    }

    on (event, callback) {
        this.addListener(event, callback);
    }

    once (event, callback) {
        const wrapper = (...args) => {
            callback(...args);

            this.removeListener(event, wrapper);
        };

        this.addListener(event, wrapper);
    }

    un (event, callback) {
        return this.removeListener(event, callback);
    }

    emit(label, ...args) {
        const listeners = this.listeners.get(label);

        if (listeners && listeners.length) {
            listeners.forEach((listener) => {
                listener(...args);
            });

            return true;
        }

        return false;
    }
}

module.exports = EventEmitter;
